import React, {useRef} from "react";
import ImperativeDemoChild from "./ImperativeDemoChild"

function ImperativeDemo() {
    const buttonref = useRef(null);

    return (
        <div>
            <button 
             onClick={() => {
                buttonref.current.toggleState();
             }}>
                Parent
            </button>
            <ImperativeDemoChild ref={buttonref}></ImperativeDemoChild>

        </div>
    )
}

export default ImperativeDemo;