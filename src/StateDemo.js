import React, { useState} from "react";

const StateDemo = () => {
    const [inputVal, setInputVal] = useState("Mudassar");

    let onChange = (event) =>{
        const newValue= event.target.value;
        setInputVal(newValue);
    };

    return (
        <div>
            <input placeholder='type to initiate...' onChange={onChange}></input>
            {inputVal}
        </div>
    );

};

export default StateDemo;