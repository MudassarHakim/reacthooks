import React,{useLayoutEffect, useRef, useEffect} from "react";

function LayoutDemo(){
    const inputRefVal = useRef(null);

    useLayoutEffect(() => {
        
        setTimeout(() => {
            inputRefVal.current.value="useLayoutEffect";
           }, 3000);
    }, []);

    useEffect(() => {
         setTimeout(() => {
            inputRefVal.current.value="useEffect";
           }, 5000);
    }, []);

    return (
        <div>
            <input type="text" ref={inputRefVal} value="React"></input>
        </div>
    );

}

export default LayoutDemo;