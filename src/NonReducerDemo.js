import React, {useState} from "react";

const NonReducerDemo = () => {
    const [count,setCount] = useState(0);
    const [showText, setShowText] = useState(true);

    return (
        <div>
            <h1>Count: {count}</h1>

            <button
                onClick={
                    ()=>{
                        setCount(count+1);
                        setShowText(!showText);
                    }
                }
            >
                Toggle
            </button>

            {showText && <p>This is a text</p>}
        </div>

    );

};

export default NonReducerDemo;