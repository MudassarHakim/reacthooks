import logo from './logo.svg';
import './App.css';
import StateDemo from './StateDemo';
import NonReducerDemo from './NonReducerDemo';
import ReducerDemo from './ReducerDemo';
import EffectSample from './EffectSample';
import EffectDemo from './EffectDemo';
import LayoutDemo from './LayoutDemo';
import ImperativeDemo from './ImperativeDemo';

function App() {
  return (
    <div className="App">
      <ImperativeDemo />
    </div>
  );
}

export default App;
