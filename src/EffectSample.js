import React, {useEffect, useState} from "react";

function EffectSample() {
    const [data, setData] = useState("");

    useEffect(() => {
        let fetchRes = fetch(
            "https://jsonplaceholder.typicode.com/todos/1");
        
        fetchRes.then(res =>
                res.json()).then(d => {
                    setData(d.title);
                    console.log(d.title);
                });
    },[]);

    return <div>Hello World {data}</div>
}

export default EffectSample;