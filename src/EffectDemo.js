import React, {useState, useEffect} from "react";

function EffectDemo() {
    const [data, setData] = useState("");
    const [count, setCount] = useState(1);

    useEffect(() => {
        let fetchRes = fetch(
            "https://jsonplaceholder.typicode.com/todos/"+count);
        
        fetchRes.then(res =>
                res.json()).then(d => {
                    setData(d.title);
                    console.log(d.title);
                });    
        
    }, [count]); // only run the effect if count changes

    return (
        <div>
            Hello Zhere commentalis
            <h1>{data}</h1>
            <h1>{count}</h1>
            <button
            onClick={() => {setCount(count+1);}}>AddCount</button>
        </div>
    );

};

export default EffectDemo;