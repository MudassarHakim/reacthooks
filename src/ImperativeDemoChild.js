import React, { useState, useImperativeHandle} from "react";
import { forwardRef } from "react";

const ImperativeDemoChild =forwardRef((props,ref)=>{

    const [toggle, setToggle] = useState(false);

    useImperativeHandle(
        ref,
        () => ({
            toggleState(){
                setToggle(!toggle);
            },
        }));

    return (
        <div>
            <button>CHild</button><br/>
            {toggle && <span>Toggle</span>}
        </div>
    );
});

export default ImperativeDemoChild;